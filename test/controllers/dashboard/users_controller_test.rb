require 'test_helper'

class Dashboard::UsersControllerTest < ActionController::TestCase
  setup do
    @dashboard_user = dashboard_users(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:dashboard_users)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create dashboard_user" do
    assert_difference('Dashboard::User.count') do
      post :create, dashboard_user: {  }
    end

    assert_redirected_to dashboard_user_path(assigns(:dashboard_user))
  end

  test "should show dashboard_user" do
    get :show, id: @dashboard_user
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @dashboard_user
    assert_response :success
  end

  test "should update dashboard_user" do
    patch :update, id: @dashboard_user, dashboard_user: {  }
    assert_redirected_to dashboard_user_path(assigns(:dashboard_user))
  end

  test "should destroy dashboard_user" do
    assert_difference('Dashboard::User.count', -1) do
      delete :destroy, id: @dashboard_user
    end

    assert_redirected_to dashboard_users_path
  end
end

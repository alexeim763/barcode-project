$(document).on('ready page:load', function () {
  $('#button-collapseMenu').sideNav();
  $('select').material_select();
  

  $('#input_barCode').on('change', function(ev) {
    var content = $(this).val();
    $("#barcode").JsBarcode(content,{format:"CODE128",displayValue:true,fontSize:20});
  });

  $("#barcode_show").JsBarcode($('#codeitem').text(),{format:"CODE128",displayValue:true,fontSize:20});

  if($('#input_barCode').val()){
     $("#barcode").JsBarcode($('#input_barCode').val(),{format:"CODE128",displayValue:true,fontSize:20});
  }
 
  $('#printBarcode').on('click', function(){
    var data = $('#contentPrint').html();
    print(data);
  });

  function print(el){
    var newPage = window.open('','_blank','width=600,height=800');
    var strHtml = "<html>\n<head>\n <link rel=\"stylesheet\" type=\"text/css\" href=\"test.css\">\n</head><body><div style=\"width: 250px;\">\n" + el + "\n</div>\n</body>\n</html>";
    console.log(strHtml);
    newPage.document.write(strHtml);
    setTimeout(function(){ 
      newPage.print();
      newPage.close();
    }, 1000);   
  }

  //Search With Ajax

  $('#searchByBarCode').on('keyup',function(){
    console.log('se cambio');
    var id = $(this).val();

    $.get( "search/"+id, function( data ) {
      console.log(data);
      $('#contentItem').html('');
      if(data != null){
         var template = ` <div class="row">
                            <div class="col s12 m7">
                              <div class="card">
                                <div class="card-image">
                                  <img src="${data.avatar_url}">
                                  <span class="card-title">${data.name}</span>
                                </div>
                                <div class="card-content">
                                  <b>Descripción: </b> <p class="">${data.description}</p>
                                  <b>Model: </b> <p class="">${data.model}</p>
                                  <b>Precio: </b> <p class="">${data.price}</p>
                                </div>
                              </div>
                            </div>
                          </div>
                      `
        }
       
        else{
          var template = `<h5>No se encontro ningun producto</h5>`
        }

      $('#contentItem').append(template);        
    });
    setTimeout(function(){
      $('#searchByBarCode').val('')
    },1000)
  });

});

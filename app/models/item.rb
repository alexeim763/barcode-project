class Item < ActiveRecord::Base
  belongs_to :category
  validates :code, uniqueness: true
  has_attached_file :avatar, styles: { medium: "500x500>"}, default_url: "/images/missing.png"
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/

  def avatar_url
    self.avatar.url(:medium)
  end

  def self.search(search)
    where("name LIKE ?", "%#{search}%") 
  end
end

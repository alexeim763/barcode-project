json.array!(@dashboard_users) do |dashboard_user|
  json.extract! dashboard_user, :id
  json.url dashboard_user_url(dashboard_user, format: :json)
end

class Dashboard::UsersController < ApplicationController
  before_action :set_dashboard_user, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /dashboard/users
  # GET /dashboard/users.json
  def index
    @dashboard_users = User.paginate(page: params[:page], per_page: 10)
  end

  # GET /dashboard/users/1
  # GET /dashboard/users/1.json
  def show
  end

  # GET /dashboard/users/new
  def new
    @dashboard_user = User.new
  end

  # GET /dashboard/users/1/edit
  def edit
  end

  # POST /dashboard/users
  # POST /dashboard/users.json
  def create
    @dashboard_user = User.new(dashboard_user_params)

    respond_to do |format|
      if @dashboard_user.save
        format.html { redirect_to dashboard_users_path, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @dashboard_user }
      else
        format.html { render :new }
        format.json { render json: @dashboard_user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /dashboard/users/1
  # PATCH/PUT /dashboard/users/1.json
  def update
    respond_to do |format|
      if @dashboard_user.update(dashboard_user_params)
        format.html { redirect_to dashboard_users_path, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @dashboard_user }
      else
        format.html { render :edit }
        format.json { render json: @dashboard_user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /dashboard/users/1
  # DELETE /dashboard/users/1.json
  def destroy
    @dashboard_user.destroy
    respond_to do |format|
      format.html { redirect_to dashboard_users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_dashboard_user
      @dashboard_user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def dashboard_user_params
       params.require(:user).permit(:email,:password)
    end
end

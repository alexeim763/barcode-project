class SearchController < ApplicationController
  before_action :authenticate_user!
  def index
  end
  def show
    id = params[:id]
    item = Item.find_by code: id
    render json: item.to_json(methods: [:avatar_url])
  end
end

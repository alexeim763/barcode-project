class ItemsController < ApplicationController
  before_action :find_item, only: [:edit, :show, :update, :destroy]
  before_action :authenticate_user!
  def index
    @items = Item.paginate(page: params[:page], per_page: 10)
    if params[:search]
      @items = Item.search(params[:search]).paginate(page: params[:page], per_page: 3).order("created_at DESC")
    else
      @items = Item.paginate(page: params[:page], per_page: 3).order('created_at DESC')
    end
  end
  
  def new
    @item = Item.new
  end

  def edit
  end

  def create
    @item = Item.new params_item
    if @item.save
      redirect_to items_path, notice: 'Se logró guardar el item'
    else
      render 'new', notice: 'Ocurrió un error'
    end 
  end

  def show
  end

  def update
    if @item.update params_item
      redirect_to items_path, notice: 'Se logro actualizar correctamente'
    else
      render 'edit', notice: 'Ocurrió un error'
    end
  end

  def destroy
    @item.destroy
    redirect_to items_path, notice: 'Se eliminó correctamente'
  end

  private
  def find_item
    @item = Item.find(params[:id])
  end
  def params_item
    params.require(:item).permit(:name, :code,  :description, :model, :category_id ,:price, :avatar)
  end

end

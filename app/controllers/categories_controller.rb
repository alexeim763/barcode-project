class CategoriesController < ApplicationController
  before_action :find_category, only: [:show, :edit, :destroy, :update]
  before_action :authenticate_user!

  def index
    @categories = Category.paginate(page: params[:page], per_page: 10)
    if params[:search]
      @categories = Category.search(params[:search]).paginate(page: params[:page], per_page: 3).order("created_at DESC")
    else
      @categories = Category.paginate(page: params[:page], per_page: 3).order('created_at DESC')
    end
  end

  def new
    @category = Category.new
  end

  def create
    @category = Category.new category_params
    if @category.save
      redirect_to categories_path, notice: "Se guardo Correctamente"
    else
      render 'new', notice: 'Oops, Ocurrio algun tipo de error'
    end
  end

  def show
  end

  def edit
  end

  def destroy
    @category.destroy
    redirect_to categories_path
  end

  def update
    if @category.update category_params
      redirect_to categories_path, notice: "Se actualizo"
    else
      redirect_to 'edit', notice: 'Algo salio Mál'
    end
  end
  
  private 
  def category_params
    params.require(:category).permit(:name)
  end
  def find_category
    @category = Category.find(params[:id])
  end
end

class ChangeColumncodeinItems < ActiveRecord::Migration
  def change
    change_column :items, :code, :string
  end
end

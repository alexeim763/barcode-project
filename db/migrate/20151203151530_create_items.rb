class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :name
      t.text :description
      t.string :model
      t.references :category, index: true, foreign_key: true
      t.integer :price

      t.timestamps null: false
    end
  end
end

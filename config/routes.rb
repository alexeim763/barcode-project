Rails.application.routes.draw do
  devise_for :users
  resources :items
  resources :categories
  namespace :dashboard do
    resources :users
  end

  get '/search', to: 'search#index'
  get '/search/:id', to: 'search#show'

  root 'items#index'

end
